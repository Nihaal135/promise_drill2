//Fetching the input data.
let inventoryDetails = require('./inventory');

function signIn(userName){
    let currentDate = new Date();
    return new Promise((resolve, reject) => {
        if(currentDate.getSeconds > 30){
            resolve();
        } else {
            reject({code : 201 , message:'Sign-in failed'});
        }
    });
}

function getBooks(){
    return new Promise((resolve, reject) => {
        if((Math.random() * 1) == 1){
            resolve(inventoryDetails);
        } else {
            reject({code : 125, message :'Service Error'});
        }
    });
}

function logData(operationDone){
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(operationDone);
        }, 1000);
    });
}

function userOperations(userName){
    let logResults = [];
    signIn(userName)
    .then(() => {
        logResults.push(logData('Sign In - Success'));
        return getBooks();
    })
    .then((books) => {
        logResults.push(logData('GetBooks - Success'));
        console.log(books);
    })
    .catch((error) => {
        logResults.push(logData(error))
        Promise.all(logResults)
        .then((data) => console.log(data))
    })
}
module.exports=userOperations;